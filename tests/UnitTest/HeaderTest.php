<?php

namespace Anakeen\Test\UnitTest;

use Anakeen\PoParser\Catalog\Catalog;
use PHPUnit\Framework\TestCase;
use Anakeen\PoParser\Parser;

class HeaderTest extends TestCase
{
    public function testGetPluralFormsCount()
    {
        $catalog = $this->parseFile();

        $this->assertEquals(3, $catalog->getHeader()->getPluralFormsCount());
    }

    /**
     * @return Catalog
     */
    protected function parseFile(): Catalog
    {
        return Parser::parseFile(\dirname(\dirname(__DIR__)).'/fixtures/basicHeadersMultiline.po');
    }
}
