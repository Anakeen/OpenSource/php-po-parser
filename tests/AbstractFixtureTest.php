<?php

namespace Anakeen\Test;

use PHPUnit\Framework\TestCase;
use Anakeen\PoParser\Catalog\Catalog;
use Anakeen\PoParser\Parser;

abstract class AbstractFixtureTest extends TestCase
{
    /** @var string */
    protected $resourcesPath;

    public function setUp(): void
    {
        $this->resourcesPath = \dirname(__DIR__).'/fixtures/';
    }

    /**
     * @param string $file
     *
     * @return Catalog
     */
    protected function parseFile($file)
    {
        //try {
            return Parser::parseFile($this->resourcesPath.$file);
        //} catch (\Exception $e) {
        //    $this->fail($e->getMessage());
        //}
    }
}
