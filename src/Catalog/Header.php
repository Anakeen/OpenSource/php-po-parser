<?php

namespace Anakeen\PoParser\Catalog;

use function array_filter;
use function array_values;
use function count;
use function preg_match;

class Header
{
    /** @var array */
    protected array $headers;

    /** @var int|null */
    protected ?int $nPlurals = null;

    public function __construct(array $headers = array())
    {
        $this->setHeaders($headers);
    }

    /**
     * @return int|null
     */
    public function getPluralFormsCount(): ?int
    {
        if ($this->nPlurals !== null) {
            return $this->nPlurals;
        }

        $header = $this->getHeaderValue('Plural-Forms');
        if ($header === null) {
            $this->nPlurals = 0;
            return $this->nPlurals;
        }

        $matches = array();
        if (preg_match('/nplurals=([0-9]+)/', $header, $matches) !== 1) {
            $this->nPlurals = 0;
            return $this->nPlurals;
        }

        $this->nPlurals = isset($matches[1]) ? (int)$matches[1] : 0;

        return $this->nPlurals;
    }

    /**
     * @param array $headers
     * @return void
     */
    public function setHeaders(array $headers)
    {
        $this->headers = $headers;
    }

    /**
     * @return array
     */
    public function asArray(): array
    {
        return $this->headers;
    }

    /**
     * @param string $headerName
     *
     * @return string|null
     */
    protected function getHeaderValue(string $headerName): ?string
    {
        $header = array_values(array_filter(
            $this->headers,
            function ($string) use ($headerName) {
                return preg_match('/'.$headerName.':(.*)/i', $string) == 1;
            }
        ));

        return count($header) ? $header[0] : null;
    }
}
