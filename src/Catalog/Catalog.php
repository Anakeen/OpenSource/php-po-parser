<?php

namespace Anakeen\PoParser\Catalog;

interface Catalog
{
    public function addEntry(Entry $entry);

    public function addHeaders(Header $headers);

    /**
     * @param string      $msgid
     * @param string|null $msgctxt
     */
    public function removeEntry(string $msgid, ?string $msgctxt = null);

    /**
     * @return array
     */
    public function getHeaders(): array;

    /**
     * @return Header
     */
    public function getHeader(): Header;

    /**
     * @return Entry[]
     */
    public function getEntries(): array;

    /**
     * @param string      $msgId
     * @param string|null $context
     *
     * @return Entry|null
     */
    public function getEntry(string $msgId, ?string $context = null): ?Entry;
}