<?php

namespace Anakeen\PoParser\Catalog;

use function count;
use function in_array;

class Entry
{
    /** @var string */
    protected string $msgId;

    /** @var string|null */
    protected ?string $msgStr;

    /** @var string|null */
    protected ?string $msgIdPlural = null;

    /** @var string[] */
    protected array $msgStrPlurals;

    /** @var string|null */
    protected ?string $msgCtxt = null;

    /** @var Entry|null */
    protected ?Entry $previousEntry = null;

    /** @var bool|null */
    protected ?bool $obsolete = null;

    /** @var array */
    protected array $flags;

    /** @var array */
    protected array $translatorComments;

    /** @var array */
    protected array $developerComments;

    /** @var array */
    protected array $reference;

    /**
     * @param string $msgId
     * @param string|null $msgStr
     */
    public function __construct(string $msgId, ?string $msgStr = null)
    {
        $this->msgId = $msgId;
        $this->msgStr = $msgStr;
        $this->msgStrPlurals = array();
        $this->flags = array();
        $this->translatorComments = array();
        $this->developerComments = array();
        $this->reference = array();
    }

    /**
     * @param string $msgId
     *
     * @return Entry
     */
    public function setMsgId(string $msgId): Entry
    {
        $this->msgId = $msgId;

        return $this;
    }

    /**
     * @param string $msgStr
     *
     * @return Entry
     */
    public function setMsgStr(string $msgStr): Entry
    {
        $this->msgStr = $msgStr;

        return $this;
    }

    /**
     * @param string $msgIdPlural
     *
     * @return Entry
     */
    public function setMsgIdPlural(string $msgIdPlural): Entry
    {
        $this->msgIdPlural = $msgIdPlural;

        return $this;
    }

    /**
     * @param string $msgCtxt
     *
     * @return Entry
     */
    public function setMsgCtxt(string $msgCtxt): Entry
    {
        $this->msgCtxt = $msgCtxt;

        return $this;
    }

    /**
     * @param null|Entry $previousEntry
     *
     * @return Entry
     */
    public function setPreviousEntry(?Entry $previousEntry): Entry
    {
        $this->previousEntry = $previousEntry;

        return $this;
    }

    /**
     * @param bool $obsolete
     *
     * @return Entry
     */
    public function setObsolete(bool $obsolete): Entry
    {
        $this->obsolete = $obsolete;

        return $this;
    }

    /**
     * @param array $flags
     *
     * @return Entry
     */
    public function setFlags(array $flags): Entry
    {
        $this->flags = $flags;

        return $this;
    }

    /**
     * @param array $translatorComments
     *
     * @return Entry
     */
    public function setTranslatorComments(array $translatorComments): Entry
    {
        $this->translatorComments = $translatorComments;

        return $this;
    }

    /**
     * @param array $developerComments
     *
     * @return Entry
     */
    public function setDeveloperComments(array $developerComments): Entry
    {
        $this->developerComments = $developerComments;

        return $this;
    }

    /**
     * @param array $reference
     *
     * @return Entry
     */
    public function setReference(array $reference): Entry
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * @param string[] $msgStrPlurals
     *
     * @return Entry
     */
    public function setMsgStrPlurals(array $msgStrPlurals): Entry
    {
        $this->msgStrPlurals = $msgStrPlurals;

        return $this;
    }

    /**
     * @return string
     */
    public function getMsgId(): string
    {
        return $this->msgId;
    }

    /**
     * @return string|null
     */
    public function getMsgStr(): ?string
    {
        return $this->msgStr;
    }

    /**
     * @return string|null
     */
    public function getMsgIdPlural(): ?string
    {
        return $this->msgIdPlural;
    }

    /**
     * @return string|null
     */
    public function getMsgCtxt(): ?string
    {
        return $this->msgCtxt;
    }

    /**
     * @return null|Entry
     */
    public function getPreviousEntry(): ?Entry
    {
        return $this->previousEntry;
    }

    /**
     * @return bool
     */
    public function isObsolete(): bool
    {
        return $this->obsolete === true;
    }

    /**
     * @return bool
     */
    public function isFuzzy(): bool
    {
        return in_array('fuzzy', $this->getFlags(), true);
    }

    /**
     * @return bool
     */
    public function isPlural(): bool
    {
        return $this->getMsgIdPlural() !== null || count($this->getMsgStrPlurals()) > 0;
    }

    /**
     * @return array
     */
    public function getFlags(): array
    {
        return $this->flags;
    }

    /**
     * @return array
     */
    public function getTranslatorComments(): array
    {
        return $this->translatorComments;
    }

    /**
     * @return array
     */
    public function getDeveloperComments(): array
    {
        return $this->developerComments;
    }

    /**
     * @return array
     */
    public function getReference(): array
    {
        return $this->reference;
    }

    /**
     * @return string[]
     */
    public function getMsgStrPlurals(): array
    {
        return $this->msgStrPlurals;
    }
}
