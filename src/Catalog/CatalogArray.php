<?php

namespace Anakeen\PoParser\Catalog;

use function md5;

class CatalogArray implements Catalog
{
    /** @var Header */
    protected Header $headers;

    /** @var array */
    protected array $entries;

    /**
     * @param Entry[] $entries
     */
    public function __construct(array $entries = array())
    {
        $this->entries = array();
        $this->headers = new Header();
        foreach ($entries as $entry) {
            $this->addEntry($entry);
        }
    }

    /**
     * @param Entry $entry
     * @return void
     */
    public function addEntry(Entry $entry)
    {
        $key = $this->getEntryHash(
            $entry->getMsgId(),
            $entry->getMsgCtxt()
        );
        $this->entries[$key] = $entry;
    }

    /**
     * @param Header $headers
     * @return void
     */
    public function addHeaders(Header $headers)
    {
        $this->headers = $headers;
    }

    /**
     * @param string $msgid
     * @param string|null $msgctxt
     * @return void
     */
    public function removeEntry(string $msgid, ?string $msgctxt = null)
    {
        $key = $this->getEntryHash($msgid, $msgctxt);
        if (isset($this->entries[$key])) {
            unset($this->entries[$key]);
        }
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers->asArray();
    }

    /**
     * @return Header
     */
    public function getHeader(): Header
    {
        return $this->headers;
    }

    /**
     * @return array
     */
    public function getEntries(): array
    {
        return $this->entries;
    }

    /**
     * @param string $msgId
     * @param string|null $context
     * @return Entry|null
     */
    public function getEntry(string $msgId, ?string $context = null): ?Entry
    {
        $key = $this->getEntryHash($msgId, $context);
        if (!isset($this->entries[$key])) {
            return null;
        }

        return $this->entries[$key];
    }

    /**
     * @param string      $msgId
     * @param string|null $context
     *
     * @return string
     */
    private function getEntryHash(string $msgId, ?string $context = null): string
    {
        return md5($msgId.$context);
    }
}
