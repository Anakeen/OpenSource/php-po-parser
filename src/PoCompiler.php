<?php

namespace Anakeen\PoParser;

use Exception;
use Anakeen\PoParser\Catalog\Catalog;
use Anakeen\PoParser\Catalog\Entry;
use Anakeen\PoParser\Catalog\Header;
use function array_unshift;
use function count;
use function explode;
use function implode;
use function max;
use function wordwrap;

class PoCompiler
{
    /** @var string */
    const TOKEN_OBSOLETE = '#~ ';

    protected int $wrappingColumn;

    protected string $lineEnding;

    protected string $tokenCarriageReturn;

    /**
     * PoCompiler constructor.
     *
     * @param int $wrappingColumn
     * @param string $lineEnding
     */
    public function __construct(int $wrappingColumn = 80, string $lineEnding = "\n")
    {
        $this->wrappingColumn = $wrappingColumn;
        $this->lineEnding = $lineEnding;
        $this->tokenCarriageReturn = chr(13);
    }

    /**
     * Compiles entries into a string
     *
     * @param Catalog $catalog
     *
     * @return string
     * @throws Exception
     * @todo Write obsolete messages at the end of the file.
     */
    public function compile(Catalog $catalog): string
    {
        $output = '';

        if (count($catalog->getHeaders()) > 0) {
            $output .= 'msgid ""'.$this->eol();
            $output .= 'msgstr ""'.$this->eol();
            foreach ($catalog->getHeaders() as $header) {
                $output .= '"'.$header.'\n"'.$this->eol();
            }
            $output .= $this->eol();
        }


        $entriesCount = count($catalog->getEntries());
        $counter = 0;
        foreach ($catalog->getEntries() as $entry) {
            if ($entry->isObsolete() === false) {
                $output .= $this->buildPreviousEntry($entry);
                $output .= $this->buildTranslatorComment($entry);
                $output .= $this->buildDeveloperComment($entry);
                $output .= $this->buildReference($entry);
            }

            $output .= $this->buildFlags($entry);

            $output .= $this->buildContext($entry);
            $output .= $this->buildMsgId($entry);
            $output .= $this->buildMsgIdPlural($entry);
            $output .= $this->buildMsgStr($entry, $catalog->getHeader());


            $counter++;
            // Avoid inserting an extra newline at end of file
            if ($counter < $entriesCount) {
                $output .= $this->eol();
            }
        }

        return $output;
    }

    /**
     * @return string
     */
    protected function eol(): string
    {
        return $this->lineEnding;
    }

    /**
     * @param $entry
     *
     * @return string
     */
    protected function buildPreviousEntry($entry): string
    {
        $previous = $entry->getPreviousEntry();
        if ($previous === null) {
            return '';
        }

        return '#| msgid '.$this->cleanExport($previous->getMsgId()).$this->eol();
    }

    /**
     * @param $entry
     *
     * @return string
     */
    protected function buildTranslatorComment($entry): string
    {
        if ($entry->getTranslatorComments() === null) {
            return '';
        }

        $output = '';
        foreach ($entry->getTranslatorComments() as $comment) {
            $output .= '# '.$comment.$this->eol();
        }

        return $output;
    }

    /**
     * @param Entry $entry
     * @return string
     */
    protected function buildDeveloperComment(Entry $entry): string
    {
        if ($entry->getDeveloperComments() === null) {
            return '';
        }

        $output = '';
        foreach ($entry->getDeveloperComments() as $comment) {
            $output .= '#. '.$comment.$this->eol();
        }

        return $output;
    }

    /**
     * @param Entry $entry
     * @return string
     */
    protected function buildReference(Entry $entry): string
    {
        $reference = $entry->getReference();
        if ($reference === null || count($reference) === 0) {
            return '';
        }

        $output = '';
        foreach ($reference as $ref) {
            $output .= '#: '.$ref.$this->eol();
        }

        return $output;
    }

    /**
     * @param Entry $entry
     * @return string
     */
    protected function buildFlags(Entry $entry): string
    {
        $flags = $entry->getFlags();
        if ($flags === null || count($flags) === 0) {
            return '';
        }

        return '#, '. implode(', ', $flags).$this->eol();
    }

    /**
     * @param Entry $entry
     * @return string
     */
    protected function buildContext(Entry $entry): string
    {
        if ($entry->getMsgCtxt() === null) {
            return '';
        }

        return
            ($entry->isObsolete() ? '#~ ' : '').
            'msgctxt '.$this->cleanExport($entry->getMsgCtxt()).$this->eol();
    }

    /**
     * @param Entry $entry
     * @return string
     */
    protected function buildMsgId(Entry $entry): string
    {
        if ($entry->getMsgId() === null) {
            return '';
        }

        return $this->buildProperty('msgid', $entry->getMsgId(), $entry->isObsolete());
    }

    /**
     * @param Entry $entry
     * @param Header $headers
     * @return string
     */
    protected function buildMsgStr(Entry $entry, Header $headers): string
    {
        $value = $entry->getMsgStr();
        $plurals = $entry->getMsgStrPlurals();

        if ($value === null && $plurals === null) {
            return '';
        }

        if ($entry->isPlural()) {
            $output = '';
            $nPlurals = $headers->getPluralFormsCount();
            $pluralsFound = count($plurals);
            $maxIterations = max($nPlurals, $pluralsFound);
            for ($i = 0; $i < $maxIterations; $i++) {
                $value = $plurals[$i] ?? '';
                $output .= $entry->isObsolete() ? self::TOKEN_OBSOLETE : '';
                $output .= 'msgstr['.$i.'] '.$this->cleanExport($value).$this->eol();
            }

            return $output;
        }

        return $this->buildProperty('msgstr', $value, $entry->isObsolete());
    }

    /**
     * @param Entry $entry
     *
     * @return string
     */
    protected function buildMsgIdPlural(Entry $entry): string
    {
        $value = $entry->getMsgIdPlural();
        if ($value === null) {
            return '';
        }

        $output = $entry->isObsolete() ? self::TOKEN_OBSOLETE : '';
        $output .= 'msgid_plural '.$this->cleanExport($value).$this->eol();
        return $output;
    }

    /**
     * @param $property
     * @param $value
     * @param bool $obsolete
     * @return string
     */
    protected function buildProperty($property, $value, bool $obsolete = false): string
    {
        $tokens = $this->wrapString($value);

        $output = '';
        if (count($tokens) > 1) {
            array_unshift($tokens, '');
        }

        foreach ($tokens as $i => $token) {
            $output .= $obsolete ? self::TOKEN_OBSOLETE : '';
            $output .= ($i === 0) ? $property.' ' : '';
            $output .= $this->cleanExport($token).$this->eol();
        }

        return $output;
    }

    /**
     * Prepares a string to be outputed into a file.
     *
     * @param string $string The string to be converted.
     *
     * @return string
     */
    protected function cleanExport(string $string): string
    {
        // only quotation mark (" or \42) and backslash (\ or \134) chars needs to be escaped
        $string = sprintf('"%s"', addcslashes($string, "\42\134"));

        // Replace newline character with \n after addcslashes to prevent escaping backslash.
        return str_replace($this->tokenCarriageReturn, '\n', $string ?? "");
    }

    /**
     * @param string $value
     * @return array
     */
    private function wrapString(string $value): array
    {
        // value that are most likely never present in the $value
        $fileSeparator = chr(28);

        /**
         * Replace newline character with the same value that is used for wrapping ($fileSeparator)
         * and with another character ($this->tokenCarriageReturn) that is later replaced back and thus
         * newline won't be escaped by addcslashes function
         */
        $value = str_replace("\n", $this->tokenCarriageReturn . $fileSeparator, $value ?? "");
        $wrapped = wordwrap($value, $this->wrappingColumn, ' ' . $fileSeparator);

        return explode(chr(28), $wrapped);
    }
}
