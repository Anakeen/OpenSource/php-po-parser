PoParser
=======
PoParser is to create or edit .po files content using PHP.  

Features
========
It supports following parsing features:

- header section.
- msgid, both single and multiline.
- msgstr, both single and multiline.
- msgctxt (Message context).
- msgid_plural (plurals forms).
- #, keys (flags).
- <span># keys (translator comments).</span>
- #. keys (Comments extracted from source code).
- #: keys (references).
- #| keys (previous strings), both single and multiline.
- #~ keys (old entries), both single and multiline.

Installation
============

```
composer require anakeen/po-parser
```

Usage
=====
```php
<?php 
// Parse a po file
$fileHandler = new Anakeen\PoParser\SourceHandler\FileSystem('es.po');

$poParser = new Anakeen\PoParser\Parser($fileHandler);
$catalog  = $poParser->parse();

// Get an entry
$entry = $catalog->getEntry('welcome.user');

// Update entry
$entry = new Entry('welcome.user', 'Welcome User!');
$catalog->setEntry($entry);

// You can also modify other entry attributes as translator comments, code comments, flags...
$entry->setTranslatorComments(array('This is shown whenever a new user registers in the website'));
$entry->setFlags(array('fuzzy', 'php-code'));
```

## Save Changes back to a file
Use `PoCompiler` together with `FileSystem` to save a catalog back to a file:
 
```php
$fileHandler = new Anakeen\PoParser\SourceHandler\FileSystem('en.po');
$compiler = new Anakeen\PoParser\PoCompiler();
$fileHandler->save($compiler->compile($catalog));
```

Documentation
=============
- [Documentation](https://github.com/raulferras/PHP-po-parser/wiki/Documentation-5.0)


Testing
=======
Tests are done using PHPUnit.
To execute tests, from command line type: 

```
php vendor/bin/phpunit
```

Credits
=======

- Raul Ferras https://github.com/raulferras  
- Anakeen dev@anakeen.com

We updated the project from [github po-parser](https://github.com/raulferras/PHP-po-parser)